# Change Log

## 1.7.0

### Minor Changes

- 2bb12c9: upgrade deps

## 1.6.1

### Patch Changes

- refactor: improve json5 rules
  fix(deps): update dependency eslint-plugin-vue to v8

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.6.0](https://github.com/sxzz/eslint-config/compare/v1.5.0...v1.6.0) (2021-10-15)

### Features

- upgrade to eslint v8 ([d6e5ab4](https://github.com/sxzz/eslint-config/commit/d6e5ab42ad04a1449f0aaa07fccd652bd8490a30))

# [1.5.0](https://github.com/sxzz/eslint-config/compare/v1.4.0...v1.5.0) (2021-10-05)

**Note:** Version bump only for package @sxzz/eslint-config-prettier

# [1.4.0](https://github.com/sxzz/eslint-config/compare/v1.3.1...v1.4.0) (2021-10-01)

**Note:** Version bump only for package @sxzz/eslint-config-prettier
