# Change Log

## 1.7.0

### Minor Changes

- 2bb12c9: upgrade deps

### Patch Changes

- Updated dependencies [2bb12c9]
  - @sxzz/eslint-config-prettier@1.7.0
  - @sxzz/eslint-config-vue@1.7.0

## 1.6.1

### Patch Changes

- refactor: improve json5 rules
  fix(deps): update dependency eslint-plugin-vue to v8
- Updated dependencies [undefined]
  - @sxzz/eslint-config-prettier@1.6.1
  - @sxzz/eslint-config-vue@1.6.1

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.6.0](https://github.com/sxzz/eslint-config/compare/v1.5.0...v1.6.0) (2021-10-15)

### Features

- upgrade to eslint v8 ([d6e5ab4](https://github.com/sxzz/eslint-config/commit/d6e5ab42ad04a1449f0aaa07fccd652bd8490a30))

# [1.5.0](https://github.com/sxzz/eslint-config/compare/v1.4.0...v1.5.0) (2021-10-05)

**Note:** Version bump only for package @sxzz/eslint-config

# [1.4.0](https://github.com/sxzz/eslint-config/compare/v1.3.1...v1.4.0) (2021-10-01)

**Note:** Version bump only for package @sxzz/eslint-config

# [1.0.0](https://github.com/sxzz/eslint-config/compare/v0.6.6...v1.0.0) (2021-06-19)

**Note:** Version bump only for package @sxzz/eslint-config

## [0.6.6](https://github.com/antfu/eslint-config/compare/v0.6.5...v0.6.6) (2021-06-11)

**Note:** Version bump only for package @antfu/eslint-config

## [0.6.5](https://github.com/antfu/eslint-config/compare/v0.6.4...v0.6.5) (2021-05-09)

**Note:** Version bump only for package @antfu/eslint-config

## [0.6.4](https://github.com/antfu/eslint-config/compare/v0.6.3...v0.6.4) (2021-04-19)

**Note:** Version bump only for package @antfu/eslint-config

## [0.6.3](https://github.com/antfu/eslint-config/compare/v0.6.2...v0.6.3) (2021-04-19)

**Note:** Version bump only for package @antfu/eslint-config

## [0.6.2](https://github.com/antfu/eslint-config/compare/v0.6.1...v0.6.2) (2021-03-15)

**Note:** Version bump only for package @antfu/eslint-config

## [0.6.1](https://github.com/antfu/eslint-config/compare/v0.6.0...v0.6.1) (2021-03-14)

**Note:** Version bump only for package @antfu/eslint-config

# [0.6.0](https://github.com/antfu/eslint-config/compare/v0.5.1...v0.6.0) (2021-03-14)

**Note:** Version bump only for package @antfu/eslint-config

## [0.5.1](https://github.com/antfu/eslint-config/compare/v0.5.0...v0.5.1) (2021-03-13)

**Note:** Version bump only for package @antfu/eslint-config

# [0.5.0](https://github.com/antfu/eslint-config/compare/v0.4.3...v0.5.0) (2021-03-13)

**Note:** Version bump only for package @antfu/eslint-config

## [0.4.3](https://github.com/antfu/eslint-config/compare/v0.4.2...v0.4.3) (2020-10-21)

**Note:** Version bump only for package @antfu/eslint-config

## [0.4.2](https://github.com/antfu/eslint-config/compare/v0.4.1...v0.4.2) (2020-10-21)

**Note:** Version bump only for package @antfu/eslint-config

## [0.4.1](https://github.com/antfu/eslint-config/compare/v0.4.0...v0.4.1) (2020-10-21)

**Note:** Version bump only for package @antfu/eslint-config

## [0.3.4](https://github.com/antfu/eslint-config/compare/v0.3.3...v0.3.4) (2020-10-20)

**Note:** Version bump only for package @antfu/eslint-config

## [0.3.3](https://github.com/antfu/eslint-config/compare/v0.3.2...v0.3.3) (2020-08-21)

**Note:** Version bump only for package @antfu/eslint-config

## [0.3.2](https://github.com/antfu/eslint-config/compare/v0.3.1...v0.3.2) (2020-08-12)

**Note:** Version bump only for package @antfu/eslint-config

## [0.3.1](https://github.com/antfu/eslint-config/compare/v0.3.0...v0.3.1) (2020-08-12)

**Note:** Version bump only for package @antfu/eslint-config

# [0.3.0](https://github.com/antfu/eslint-config/compare/v0.2.14...v0.3.0) (2020-08-12)

### Features

- add react and all ([a7fbdca](https://github.com/antfu/eslint-config/commit/a7fbdcad4b20294e26e817fae468f468376e49cf))
